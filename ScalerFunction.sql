CREATE OR ALTER FUNCTION dbo.getHallWeekendId(@hall_id INT)
RETURNS INT
AS 
BEGIN
	DECLARE @weekend_id INT;
	DECLARE @weekend VARCHAR(10);

	DECLARE @list_of_weekdays TABLE (id INT, day_name VARCHAR(10)); 

	INSERT INTO @list_of_weekdays VALUES
	(1, 'Sunday'),
	(2, 'Monday'),
	(3, 'Tuesday'),
	(4, 'Wednesday'),
	(5, 'Thursday'),
	(6, 'Friday'),
	(7, 'Saturday');

	SET @weekend = (SELECT day_name FROM @list_of_weekdays EXCEPT (SELECT DISTINCT(DATENAME(WEEKDAY, day)) FROM schedule WHERE hall_id = @hall_id));

	SET @weekend_id = (SELECT id FROM @list_of_weekdays WHERE day_name = @weekend);
	
	RETURN @weekend_id;
END;