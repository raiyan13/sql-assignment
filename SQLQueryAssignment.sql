CREATE DATABASE online_movie_booking_system_database;

CREATE TABLE movie(
	id INT IDENTITY NOT NULL PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	release_date DATE NOT NULL,
	rating DECIMAL(2,1) DEFAULT 0.0,
	CONSTRAINT CHK_rating CHECK (rating>=0.0 AND rating<=5.0)
); 

CREATE TABLE movie_schedule(
	id INT IDENTITY NOT NULL PRIMARY KEY,
	movie_id INT NOT NULL,
	schedule_id INT NOT NULL,
	date DATE NOT NULL,
	ticket_price INT NOT NULL
); 

CREATE TABLE hall(
	id INT IDENTITY NOT NULL PRIMARY KEY,
	cinema_id INT NOT NULL,
	name VARCHAR(70) NOT NULL,
	capacity INT NOT NULL
);

CREATE TABLE cinema(
	id INT IDENTITY NOT NULL PRIMARY KEY,
	city_id INT NOT NULL,
	name VARCHAR(70) NOT NULL
);

CREATE TABLE schedule(

	id INT IDENTITY NOT NULL PRIMARY KEY,
	hall_id INT NOT NULL,
	day DATE NOT NULL,
	start_time TIME NOT NULL, 
	end_time TIME NOT NULL,
	CONSTRAINT CHK_movie_duration CHECK (DATEDIFF(hour, start_time, end_time) < 4)

);

CREATE TABLE booking(
	id INT IDENTITY NOT NULL PRIMARY KEY,
	movie_schedule_id INT NOT NULL,
	user_id INT NOT NULL,
	number_of_seat INT NOT NULL,
	CONSTRAINT CHK_number_of_seat CHECK (number_of_seat > 0)
); 

CREATE TABLE city(
	id INT IDENTITY NOT NULL PRIMARY KEY,
	name VARCHAR(50) NOT NULL
);

CREATE TABLE tbl_user(
	id INT IDENTITY NOT NULL PRIMARY KEY,
	city_id INT NOT NULL,
	firstname VARCHAR(30) NOT NULL,
	lastname VARCHAR(30) NOT NULL,
	gender VARCHAR(6) NOT NULL,
	dateofbirth DATE NOT NULL,
	CONSTRAINT CHK_gender CHECK (gender in ('Male', 'Female', 'Other'))
);


ALTER TABLE movie_schedule
ADD CONSTRAINT FK_movie_schedule_movie 
FOREIGN KEY (movie_id) REFERENCES movie(id) ON DELETE CASCADE;

ALTER TABLE movie_schedule
ADD CONSTRAINT FK_movie_schedule_schedule 
FOREIGN KEY (schedule_id) REFERENCES schedule(id) ON DELETE CASCADE;

ALTER TABLE hall
ADD CONSTRAINT FK_hall_cinema 
FOREIGN KEY (cinema_id) REFERENCES cinema(id) ON DELETE CASCADE;

ALTER TABLE cinema
ADD CONSTRAINT FK_cinema_city 
FOREIGN KEY (city_id) REFERENCES city(id) ON DELETE CASCADE;

ALTER TABLE schedule
ADD CONSTRAINT FK_schedule_hall
FOREIGN KEY (hall_id) REFERENCES hall(id) ON DELETE CASCADE;

ALTER TABLE booking
ADD CONSTRAINT FK_booking_movie_schedule
FOREIGN KEY (movie_schedule_id) REFERENCES movie_schedule(id);

ALTER TABLE booking
ADD CONSTRAINT FK_booking_user
FOREIGN KEY (user_id) REFERENCES tbl_user(id) ON DELETE CASCADE;

ALTER TABLE tbl_user
ADD CONSTRAINT FK_user_city
FOREIGN KEY (city_id) REFERENCES city(id);


INSERT INTO city (name) VALUES
('City_1'), 
('City_2'),
('City_3'),
('City_4'),
('City_5'),
('City_6'),
('City_7'),
('City_8'),
('City_9');


INSERT INTO cinema (city_id, name) VALUES
(1, 'Cinema_1'),
(2, 'Cinema_2'),
(3, 'Cinema_3'),
(4, 'Cinema_4'),
(5, 'Cinema_5'),
(6, 'Cinema_6'),
(7, 'Cinema_7'),
(8, 'Cinema_8'),
(9, 'Cinema_9');

INSERT INTO hall(cinema_id, name, capacity) VALUES
(1, 'Cinema_1_hall_1', 50),
(1, 'Cinema_1_hall_2', 30),
(2, 'Cinema_2_hall_1', 40),
(3, 'Cinema_3_hall_1', 30),
(3, 'Cinema_3_hall_2', 40),
(4, 'Cinema_4_hall_1', 20),
(4, 'Cinema_4_hall_2', 10),
(5, 'Cinema_5_hall_1', 70),
(6, 'Cinema_6_hall_1', 60),
(7, 'Cinema_7_hall_1', 40),
(8, 'Cinema_8_hall_1', 30),
(8, 'Cinema_8_hall_2', 45),
(8, 'Cinema_8_hall_3', 34),
(9, 'Cinema_9_hall_1', 52),
(9, 'Cinema_9_hall_2', 51);

INSERT INTO schedule(hall_id, day, start_time, end_time) VALUES
(1, '2022-02-06', '2:00:00', '4:30:00'),
(1, '2022-02-07', '9:00:00', '12:00:00'),
(1, '2022-02-08', '5:00:00', '8:30:00'),
(1, '2022-02-09', '9:00:00', '12:00:00'),
(1, '2022-02-10', '3:00:00', '5:30:00'),
(1, '2022-02-11', '5:00:00', '8:30:00'),
(1, '2022-02-13', '5:00:00', '8:30:00'),

(3, '2022-02-05','9:00:00', '12:00:00'),
(3, '2022-02-07','9:00:00', '12:00:00'),
(3, '2022-02-08','5:00:00', '8:30:00'),
(3, '2022-02-09','9:00:00', '12:00:00'),
(3, '2022-02-10','3:00:00', '5:30:00'),
(3, '2022-02-11','5:00:00', '8:30:00'),
(3, '2022-02-12','5:00:00', '8:30:00'),
(3, '2022-02-14','5:00:00', '8:30:00'),

(4, '2022-02-05','9:00:00', '12:00:00'),
(4, '2022-02-06','2:00:00', '4:30:00'),
(4, '2022-02-08','5:00:00', '8:30:00'),
(4, '2022-02-09','9:00:00', '12:00:00'),
(4, '2022-02-10','3:00:00', '5:30:00'),
(4, '2022-02-11','5:00:00', '8:30:00'),

(6, '2022-02-05','9:00:00', '12:00:00'),
(6, '2022-02-06','2:00:00', '4:30:00'),
(6, '2022-02-07','9:00:00', '12:00:00'),
(6, '2022-02-09','9:00:00', '12:00:00'),
(6, '2022-02-10','3:00:00', '5:30:00'),
(6, '2022-02-11','5:00:00', '8:30:00'),

(8, '2022-02-05','9:00:00', '12:00:00'),
(8, '2022-02-06','2:00:00', '4:30:00'),
(8, '2022-02-07','9:00:00', '12:00:00'),
(8, '2022-02-08','5:00:00', '8:30:00'),
(8, '2022-02-10','3:00:00', '5:30:00'),
(8, '2022-02-11','5:00:00', '8:30:00'),

(9, '2022-02-05','9:00:00', '12:00:00'),
(9, '2022-02-06','2:00:00', '4:30:00'),
(9, '2022-02-07','9:00:00', '12:00:00'),
(9, '2022-02-08','5:00:00', '8:30:00'),
(9, '2022-02-9','9:00:00', '12:00:00'),
(9, '2022-02-11','5:00:00', '8:30:00'),

(10, '2022-02-05','9:00:00', '12:00:00'),
(10, '2022-02-06','2:00:00', '4:30:00'),
(10, '2022-02-07','9:00:00', '12:00:00'),
(10, '2022-02-08','5:00:00', '8:30:00'),
(10, '2022-02-09','9:00:00', '12:00:00'),
(10, '2022-02-10','3:00:00', '5:30:00'),

(11, '2022-02-06','2:00:00', '4:30:00'),
(11, '2022-02-07','9:00:00', '12:00:00'),
(11, '2022-02-08','5:00:00', '8:30:00'),
(11, '2022-02-09','9:00:00', '12:00:00'),
(11, '2022-02-10','3:00:00', '5:30:00'),
(11, '2022-02-11','5:00:00', '8:30:00'),

(14, '2022-02-05','9:00:00', '12:00:00'),
(14, '2022-02-07','9:00:00', '12:00:00'),
(14, '2022-02-08','5:00:00', '8:30:00'),
(14, '2022-02-09','9:00:00', '12:00:00'),
(14, '2022-02-10','3:00:00', '5:30:00'),
(14, '2022-02-11','5:00:00', '8:30:00'),

(2, '2021-05-04','9:00:00', '12:00:00'),
(2, '2021-05-04','13:00:00', '16:00:00'),
(2, '2022-05-06','9:00:00', '12:00:00'),
(2, '2022-05-07','3:00:00', '5:30:00'),
(2, '2022-05-08','5:00:00', '8:30:00'),
(2, '2022-05-09','5:00:00', '8:30:00'),
(2, '2022-05-10','5:00:00', '8:30:00'),

(12, '2022-02-06','2:00:00', '4:30:00'),
(12, '2022-02-07','9:00:00', '12:00:00'),
(12, '2022-02-08','5:00:00', '8:30:00'),
(12, '2022-02-09','9:00:00', '12:00:00'),
(12, '2022-02-10','3:00:00', '5:30:00'),
(12, '2022-02-11','5:00:00', '8:30:00'),

(13, '2022-02-06','2:00:00', '4:30:00'),
(13, '2022-02-07','9:00:00', '12:00:00'),
(13, '2022-02-08','5:00:00', '8:30:00'),
(13, '2022-02-09','9:00:00', '12:00:00'),
(13, '2022-02-10','3:00:00', '5:30:00'),
(13, '2022-02-11','5:00:00', '8:30:00');


INSERT INTO movie(name, release_date, rating) VALUES
('Movie_1', '2021-05-04', 3.4),
('Movie_2', '2021-06-01', 4.4),
('Movie_3', '2021-07-02', 5.0),
('Movie_4', '2021-09-04', 3.5),
('Movie_5', '2021-08-01', 4.4),
('Movie_6', '2021-07-21', 4.0),
('Movie_7', '2021-06-05', 5.0),
('Movie_8', '2021-05-04', 5.0),
('Movie_9', '2020-10-05', 3.4),
('Movie_10', '2022-02-06', 0.0),
('Movie_11', '2022-02-06', 0.0);


INSERT INTO movie_schedule(movie_id, schedule_id, date, ticket_price) VALUES
(1, 1, '2022-02-25', 500),
(2, 14, '2022-02-24', 500),
(3, 16, '2022-02-23', 550),
(4, 20, '2022-02-25', 600),
(5, 25, '2022-02-23', 250),
(6, 35, '2022-02-25', 500),
(7, 41, '2022-02-22', 350),
(8, 48, '2022-02-25', 400),
(9, 50, '2022-02-25', 500),
(1, 58, '2022-02-25', 530),
(8, 59, '2022-02-25', 530),
(11, 26, '2022-02-23', 550),
(11, 65, '2022-02-01', 550),
(11, 71, '2022-02-01', 550);


INSERT INTO tbl_user(city_id, firstname, lastname, gender, dateofbirth) VALUES
(1, 'KAMAL', 'AHMED', 'Male', '1996-10-12'),
(1, 'JAMAL', 'AHMED', 'Male', '1997-03-11'),
(2, 'HARIS', 'ALI', 'Male', '1994-10-10'),
(3, 'HALIMA', 'BEGUM', 'Female', '1995-06-05'),
(4, 'RAHIMA', 'KHATUN', 'Female', '1993-10-02'),
(4, 'RATAN', 'AHMED', 'Male', '1992-10-25'),
(5, 'KASHEM', 'ALI', 'Male', '1991-12-21'),
(6, 'KISHOR', 'AHMED', 'Male', '1990-02-12'),
(6, 'MUSA', 'AHMED', 'Male', '1976-05-20'),
(7, 'ROBIN', 'HOSSAIN', 'Male', '1986-10-12'),
(8, 'TAHMINA', 'PARVIN', 'Female', '1976-04-23'),
(9, 'ASADUL', 'KABIR', 'Male', '1986-10-12'),
(9, 'RIYAD', 'UDDIN', 'Male', '1989-12-25'),
(9, 'PIKU', 'AHMED', 'Male', '1975-11-12');


INSERT INTO booking(movie_schedule_id, user_id, number_of_seat) VALUES
(2, 1, 20),
(2, 2, 20),
(2, 3, 10);


--Query 1
SELECT hall.cinema_id, movie.name
FROM (((movie 
INNER JOIN movie_schedule ON movie.id = movie_schedule.movie_id)
INNER JOIN schedule ON movie_schedule.schedule_id = schedule.id)
INNER JOIN hall ON schedule.hall_id = hall.id) GROUP BY cinema_id, movie.name;

--Query 2
SELECT hall.cinema_id, COUNT(movie.id) AS number_of_movies
FROM (((movie 
INNER JOIN movie_schedule ON movie.id = movie_schedule.movie_id)
INNER JOIN schedule ON movie_schedule.schedule_id = schedule.id)
INNER JOIN hall ON schedule.hall_id = hall.id) WHERE YEAR(schedule.day) = YEAR(getdate()) OR MONTH(schedule.day) = MONTH(getdate()) GROUP BY cinema_id, movie.name;

--Query 3
SELECT * FROM schedule WHERE id NOT IN (SELECT schedule_id FROM movie_schedule);

--Function
SELECT dbo.getHallWeekendId (1) AS Weekend_id;

--Stored Procedure
DECLARE @MyOutput INT;
EXEC @MyOutput = sp_book_movie_ticket @UserId = 1, @NumberOfTickets = 2, @MovieScheduleId = 3;
PRINT @MyOutput;
