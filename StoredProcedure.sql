CREATE OR ALTER PROCEDURE sp_book_movie_ticke @UserId INT, @NumberOfTickets INT, @MovieScheduleId INT
AS
BEGIN
	DECLARE @total_occupied_seats INT;
	DECLARE @hall_id INT;
	DECLARE @total_seat_of_hall INT;

	SET @hall_id = (SELECT hall_id FROM schedule WHERE id = (SELECT schedule_id FROM movie_schedule WHERE id = @MovieScheduleId));
	
	SET @total_seat_of_hall = (SELECT capacity FROM hall WHERE id = @hall_id);
	
	SET @total_occupied_seats =  (SELECT sum(number_of_seat) FROM booking WHERE movie_schedule_id = @MovieScheduleId);
	
	IF (@total_occupied_seats IS NULL OR @total_occupied_seats = '')
		SET @total_occupied_seats = 0;
	
	IF @NumberOfTickets > 4
		RETURN -2;

	IF (@total_occupied_seats + @NumberOfTickets) <= @total_seat_of_hall
		BEGIN
		DECLARE @total_ticket_price INT;
		SET @total_ticket_price = (SELECT ticket_price FROM movie_schedule WHERE id = @MovieScheduleId) * @NumberOfTickets;
		RETURN @total_ticket_price;
		END
	ELSE
		RETURN -1;
END;